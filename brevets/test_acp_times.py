import acp_times
import arrow

d = arrow.get("01/01/2019 00:00", "MM/DD/YYYY HH:mm")
fmt = "MM/DD/YYYY HH:mm"

def test_rusa_example():
    """
    Utilize example one from the website and ensure all is accurate
    :return:
    """
    assert acp_times.open_time(0, 200, d) == "2019-01-01T00:00:00+00:00"
    assert acp_times.close_time(0, 200, d) == "2019-01-01T01:00:00+00:00"

    assert acp_times.open_time(60, 200, d) == "2019-01-01T01:46:00+00:00"
    assert acp_times.close_time(60, 200, d) == "2019-01-01T04:00:00+00:00"

    assert acp_times.open_time(120, 200, d) == "2019-01-01T03:32:00+00:00"
    assert acp_times.close_time(120, 200, d) == "2019-01-01T08:00:00+00:00"

    assert acp_times.open_time(175, 200, d) == "2019-01-01T05:09:00+00:00"
    assert acp_times.close_time(175, 200, d) == "2019-01-01T11:40:00+00:00"

    assert acp_times.open_time(205, 200, d) == "2019-01-01T05:53:00+00:00"
    assert acp_times.close_time(205, 200, d) == "2019-01-01T13:30:00+00:00"

def test_0km_acp_times():
    """
    Control at zero should be (start_time, start_time + 1 hour)
    """
    assert acp_times.open_time(0, 200, d) == "2019-01-01T00:00:00+00:00"
    assert acp_times.close_time(0, 200, d) == "2019-01-01T01:00:00+00:00"


def test_close_times():
    """
    Check all of the close times to ensure they are working correctly.
    :return:
    """
    assert acp_times.close_time(200, 200, d) == "2019-01-01T13:30:00+00:00"
    assert acp_times.close_time(400, 200, d) == "2019-01-02T03:00:00+00:00"
    assert acp_times.close_time(600, 200, d) == "2019-01-02T16:00:00+00:00"
    assert acp_times.close_time(1000, 200, d) == "2019-01-04T03:00:00+00:00"


def test_200km_acp_times():
    """
    Testing various control distances for 200km brevet
    """
    assert acp_times.open_time(1, 200, d) == "2019-01-01T00:02:00+00:00"
    assert acp_times.close_time(1, 200, d) == "2019-01-01T00:04:00+00:00"

    assert acp_times.open_time(50, 200, d) == "2019-01-01T01:28:00+00:00"
    assert acp_times.close_time(50, 200, d) == "2019-01-01T03:20:00+00:00"

    assert acp_times.open_time(199, 200, d) == "2019-01-01T05:51:00+00:00"
    assert acp_times.close_time(199, 200, d) == "2019-01-01T13:16:00+00:00"

    assert acp_times.open_time(200, 200, d) == "2019-01-01T05:53:00+00:00"
    assert acp_times.close_time(200, 200, d) == "2019-01-01T13:30:00+00:00"

    assert acp_times.open_time(205, 200, d) == "2019-01-01T05:53:00+00:00"
    assert acp_times.close_time(205, 200, d) == "2019-01-01T13:30:00+00:00"


def test_1000km_acp_times():
    """
    Testing various control distances for a 1000km brevet
    """

    assert acp_times.open_time(200, 1000, d) == "2019-01-01T05:53:00+00:00"
    assert acp_times.close_time(200, 1000, d) == "2019-01-01T13:20:00+00:00"

    assert acp_times.open_time(250, 1000, d) == "2019-01-01T07:27:00+00:00"
    assert acp_times.close_time(250, 1000, d) == "2019-01-01T16:40:00+00:00"

    assert acp_times.open_time(400, 1000, d) == "2019-01-01T12:08:00+00:00"
    assert acp_times.close_time(400, 1000, d) == "2019-01-02T02:40:00+00:00"

    assert acp_times.open_time(450, 1000, d) == "2019-01-01T13:48:00+00:00"
    assert acp_times.close_time(450, 1000, d) == "2019-01-02T06:00:00+00:00"

    assert acp_times.open_time(600, 1000, d) == "2019-01-01T18:48:00+00:00"
    assert acp_times.close_time(600, 1000, d) == "2019-01-02T16:00:00+00:00"

    assert acp_times.open_time(650, 1000, d) == "2019-01-01T20:35:00+00:00"
    assert acp_times.close_time(650, 1000, d) == "2019-01-02T20:23:00+00:00"

    assert acp_times.open_time(890, 1000, d) == "2019-01-02T05:09:00+00:00"
    assert acp_times.close_time(890, 1000, d) == "2019-01-03T17:23:00+00:00"

    assert acp_times.open_time(999, 1000, d) == "2019-01-02T09:03:00+00:00"
    assert acp_times.close_time(999, 1000, d) == "2019-01-04T02:55:00+00:00"

    assert acp_times.open_time(1000, 1000, d) == "2019-01-02T09:05:00+00:00"
    assert acp_times.close_time(1000, 1000, d) == "2019-01-04T03:00:00+00:00"
