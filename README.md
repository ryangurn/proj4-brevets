# Project 4: Brevet time calculator with Ajax

Author: Ryan Gurnick
Email: rgurnick@uoregon.edu

Credits to Michal Young for the initial version of this code.

## Non Technical Documentation
* (https://rusa.org/pages/acp-brevet-control-times-calculator)[https://rusa.org/pages/acp-brevet-control-times-calculator]
* (https://rusa.org/pages/rulesForRiders)[https://rusa.org/pages/rulesForRiders] Article 9 for Overall End Times
* (https://arrow.readthedocs.io/en/latest/)[https://arrow.readthedocs.io/en/latest/]


## Setup
1. Running `.\run.sh` will delete all previous images and build a new one. **You must ensure that the credentials.ini file is in the `brevets` folder**
2. Run the newly created container that was built.

## Final Closing Times
1000 km = 75:00
600 km = 40:00
400 km = 27:00
300 km = 20:00
200 km = 13:30


## Build
Using docker you can build and run it in the following way.
Please note: during the build process there is automation implemented to automatically run the `nosetests` command and check that all tests passed.
```bash
> cd brevets
> docker build .
> docker run <containerID>
```

## Testing
A suite of nose test cases has been implemented and can be run using the following.
Please note: this will automatically be run when issuing the `docker build .` command.
```bash
> pip install nose
> cd brevets
> nosetests
```
There are five test cases as per the requirements

## Tasks
What I have: 
* The working application.

* A README.md file that includes not only identifying information (your name, email, etc.) but but also a revised, clear specification of the brevet controle time calculation rules.

* An automated 'nose' test suite.

* Dockerfile
